Opencast 2.3: Release Notes
===========================

**Opencast 2.3 Release Schedule**

|Date                               |Phase
|-----------------------------------|---------------------------------
|October <del>*1st*</del> 4th       |Feature Freeze
|October <del>*1st*</del> 4th - 23th|Internal QA and bug fixing phase
|October 10th - 16th                |Review Test Cases
|October 17th - 23th                |Documentation Review
|October 24th - November 14th       |Public QA phase
|November 15th - 28th               |Additional bug fixing phase
|November 21th - 27th               |Translation week
|November 28th - December 11th      |Final QA phase
|December 13th                      |Release of Opencast 2.3
